﻿using System;
using System.Threading;
using System.Timers;

namespace BDA
{
    class Program
    {

        private static System.Timers.Timer testTimer = null;

        public static void Main(string[] args)
        {
            // Atrapo el ^C
            AutoResetEvent flag = new AutoResetEvent(false);
            Console.CancelKeyPress += delegate (object sender, ConsoleCancelEventArgs e) {
                e.Cancel = true;
                flag.Set();
            };

            // Test: inicializo la placa Arduino

            //Reles.Open(3);
            //Reles.Open(4);
            Reles.Error(3);

            // Arranco el lazo ppal
            ThreadStart start = new ThreadStart(mainLoop);
            Thread mainThread = new Thread(start);
            mainThread.Start();

            // Si estoy en modo test, preparo el timer
            if (Properties.Settings.Default.testMode)
            {
                testTimer = new System.Timers.Timer();
                testTimer.Elapsed += new ElapsedEventHandler(onTestTimer);
                testTimer.Interval = Properties.Settings.Default.testIntervalo;
                testTimer.Enabled = false;
            }

            // Me fijo si hay un tiempo máximo de vida (maxTTL en segundos)
            int maxTTL = Properties.Settings.Default.maxTTL;
            if (maxTTL != -1)
                maxTTL *= 1000;

            // Espero por el ^C o expiración
            flag.WaitOne(maxTTL);

            // Listo, a matar el lazo ppal
            // TODO: hacerlo un poco mas elegante ...
            mainThread.Abort();

        }

        /// <summary>
        /// Timer para testeos (cuando no hay un scanner)
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private static void onTestTimer(object source, ElapsedEventArgs e)
        {
            string dir = System.AppDomain.CurrentDomain.BaseDirectory;
            string file = dir + Properties.Settings.Default.testFile;
            try
            {
                FPCore.getInstance().LoadImageFromFile(file, 500);
            }
            catch (Exception ex)
            {
                // Nada, está solo porque en el modo trial espera 
                // 5 segundos y no se sincroniza
            }
        }

        /// <summary>
        /// Lazo ppal
        /// TODO: algún mensaje mas elegante ante la muerte del proceso
        /// </summary>
        private static void mainLoop()
        {
            int deltaT = Properties.Settings.Default.granularidadLoop;

            // Sincronizacion con el FPCore
            // AutoResetEvent flag = new AutoResetEvent(false);
            FPCore.start();
            // flag.WaitOne();

            if (Properties.Settings.Default.testMode)
                testTimer.Start();
            try
            {
                while (true)
                {
                    // Entrego el control
                    Thread.Sleep(deltaT);

                    // Me fijo si es hora de recargar las huellas
                    FPCore.checkForDB();
                }
                
            } catch(ThreadAbortException ae)
            {
                Console.WriteLine("Finalizando el proceso ppal ...");            
            }
            finally
            {
                FPCore.shutdown();
            }
        }
    }
}
