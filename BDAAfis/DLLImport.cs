﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
//using System.Threading.Tasks;
using GriauleFingerprintLibrary.DataTypes;

namespace BDA
{
    class DLLImport
    {
        [DllImport("GrFinger.dll", EntryPoint = "GrConvertTemplate")]
        //int __stdcall GrConvertTemplate(char* oldTpt, char* newTpt, int* newTptSize, int context, int format);
        public static extern int GrConvertTemplate(byte[] oldPt, byte[] newTpt, out int newTptSize, int context, int format);

        public static FingerprintTemplate convertTemplate(FingerprintTemplate inp)
        {
            FingerprintTemplate aux = new FingerprintTemplate();
            FingerprintTemplate ret = new FingerprintTemplate();
            int newSize = aux.Size;
            int retVal = GrConvertTemplate(inp.Buffer, aux.Buffer, out newSize, GriauleFingerprintLibrary.FingerprintConstants.GR_DEFAULT_CONTEXT, (int)GriauleFingerprintLibrary.GrTemplateFormat.GR_FORMAT_DEFAULT);
            if (retVal != GriauleFingerprintLibrary.FingerprintConstants.GR_OK)
                return inp;
            ret.Buffer = aux.Buffer.Take(newSize).ToArray<byte>();
            ret.Size = newSize;
            return ret;
        }
    }
}
