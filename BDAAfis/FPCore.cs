﻿using GriauleFingerprintLibrary;
using GriauleFingerprintLibrary.DataTypes;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BDA
{
    /// <summary>
    /// Singleton de FingerPrintCore
    /// </summary>
    /// 
    class FPCore
    {
        private static int umbral = Properties.Settings.Default.umbral;

        private static Huellas fpList = null;

        private static int nHuellas = 0;
        private static int nHuellasPositivas = 0;
        private static int nHuellasFalsosNegativos = 0;

        // La magia del singleton
        private static volatile FingerprintCore fpCore = null;
        private static object syncRoot = new Object();

        /// <summary>
        /// Singleton de core fingerprint
        /// </summary>
        /// <returns>El núcleo de fp</returns>
        public static FingerprintCore getInstance()
        {
            if ( fpCore == null)
            {
                lock (syncRoot)
                {
                    if (fpCore == null)
                    {

                        fpCore = new FingerprintCore();

                        // Los eventos
                        fpCore.onImage += new ImageEventHandler(onImage);
                        fpCore.onStatus += new StatusEventHandler(onStatus);

                        // Inicializo el núcleo               
                        fpCore.Initialize();
                        fpCore.CaptureInitialize();

                        checkForDB();

                    }
                }
            }

            return fpCore;
        }
        

     
        /// <summary>
        /// Handler del onImage
        /// </summary>
        /// <param name="source"></param>
        /// <param name="ie"></param>
        private static void onImage(object source, GriauleFingerprintLibrary.Events.ImageEventArgs ie)
        {
            FingerprintRawImage ri = ie.RawImage;
            Console.WriteLine("Evento onImage lector {0}", source.ToString());
            nHuellas++;

            try 
            {
                if (Monitor.TryEnter(fpList))
                {
                    // Si lo pudo lockear, es que no hay un subthread corriendo
                    Thread thIdentify = new Thread(wIdentify);

                    object[] param = new object[2];
                    param[0] = source;
                    param[1] = ri;

                    thIdentify.Start(param);

                    Monitor.Exit(fpList);

                }
                else
                {
                    // No pudo lockear, así que quiere decir que hay
                    // un thread ya corriendo que tiene lockeada la lista de huellas.
                    // No hacer nada.
                    Console.WriteLine("Huella ignorada");
                }

            } catch(Exception ex)
            {
                // Alguna otra forma de error
                // No hacer nada.
                try 
	            {	        
                            Console.WriteLine($"Huella ignorada {ex.Message}");
		
	            }
	            catch (Exception)
	            {
                            Console.WriteLine($"Huella ignorada - error indeterminado");
	            }
            }
        }

        

        private static void wIdentify(object rawData)
        {

            object[] data = (object[])rawData;

            FingerprintRawImage ri = (FingerprintRawImage)data[1];
            if (ri != null && fpList!= null)
            {
                lock (fpList)
                {
                    try
                    {
                        // Acá es donde ocurre toda la magia!                    

                        Stopwatch st = new Stopwatch();
                        st.Start();
                        Console.WriteLine("Extrayendo template ...");

                        FingerprintTemplate fpTemplate = null;
                        getInstance().Extract(ri, ref fpTemplate);
                        //getInstance().ExtractEx(ri, ref fpTemplate, GrTemplateFormat.GR_FORMAT_DEFAULT);

                        int nCores = Properties.Settings.Default.nCores;
                        Console.WriteLine("Buscando en la lista con {0} núcleos...", nCores);

                        Huellas[] lista = fpList.getParts(nCores);
                        Huella[] resultados = new Huella[nCores];

                        Parallel.For(0, nCores,
                            (i) => { resultados[i] = lista[i].compareTo(fpTemplate); }
                            );

                        Huella result = resultados[0];
                        // Ahora si, busco el mejor
                        for (int i = 1; i < nCores; i++)
                        {
                            if (resultados[i].Score > result.Score)
                                result = resultados[i];
                        }
                        Console.WriteLine("Score máximo -> {0}", result.Score);
                        if (result.Score < Properties.Settings.Default.umbral)
                            result = null;
                        st.Stop();
                        if (result == null)
                        {
                            Console.WriteLine("Huella no encontrada!");
                            nHuellasFalsosNegativos++;
                            Reles.Error(Huellas.GetIdeOpe(data[0].ToString()));
                            // TODO: grabar el evento en la BD

                        }
                        else
                        {
                            Console.WriteLine("Huella encontrada {{{0}}},Score -> {1}", result.ToString(), result.Score);
                            Huellas.registrarAcceso(data[0].ToString(), result.ideUsu.ToString(), result.usuPer.ToString());
                            nHuellasPositivas++;
                            // TODO: grabar el evento en la BD
                        }
                        Console.WriteLine("Tiempo de búsqueda: {0} ms para {1} huellas ", st.ElapsedMilliseconds, fpList.FpLst.Count);
                        if (Properties.Settings.Default.forceGC)
                            GC.Collect();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error buscando huellas! {0}", e.StackTrace);
                        Reles.Error();
                    }
                }
            }

            Console.WriteLine(nHuellas + " huella(s) / " + nHuellasPositivas + " huella(s) Positivas / " + nHuellasFalsosNegativos + " huella(s) falsos Negativos");
            
        }


        /// <summary>
        /// Handler del onStatus
        /// </summary>
        /// <param name="source"></param>
        /// <param name="se"></param>
        private static void onStatus(object source, GriauleFingerprintLibrary.Events.StatusEventArgs se)
        {
            if (se.StatusEventType == GriauleFingerprintLibrary.Events.StatusEventType.SENSOR_PLUG)
            {
                getInstance().StartCapture(source.ToString());
            }
            else
            {
                getInstance().StopCapture(source);
            }
        }

        /// <summary>
        /// Inicializa las variables del núcleo
        /// </summary>
        public static void start()
        {
            getInstance();
            // flag.Set();
        }

        /// <summary>
        /// Finalización del singleton
        /// </summary>
        public static void shutdown()
        {
            getInstance().CaptureFinalize();
            getInstance().Finalizer();
            fpCore = null;
        }

        /// <summary>
        /// Chequeo si es necesario recargar las huellas
        /// </summary>
        public static void checkForDB()
        {
            // Invoco el chequeo en otro thread para poder hacer los lockeos.
            ThreadStart start = new ThreadStart(lockCheckForDB);
            Thread t = new Thread(start);
            t.Start();
            t.Join();

        }

        /// <summary>
        /// Versión thread safe de la carga de la base de datos
        /// </summary>
        private static void lockCheckForDB()
        {
            // Cargo la lista de huellas a memoria
            // Debiera haber una manera de recargar 
            // Cuando hay enrolamientos.
            // Consultar con Diego
            bool needLoad = false;
            if (fpList == null)
            {
                fpList = new Huellas();
                needLoad = true;
            }
            else
                needLoad = fpList.loadNeeded();

            if (needLoad)
            {
                lock (fpList)
                {
                    fpList.loadFromDB();
                }
            }
        }
    }
}

