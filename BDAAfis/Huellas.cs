﻿using GriauleFingerprintLibrary.DataTypes;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace BDA
{
    class Huellas
    {
        private static string connStr = Properties.Settings.Default.stringConn;

        private List<Huella> fpLst = null;

        private DateTime timeLoaded;

        private int maxIdeTem = -1;

        /// <summary>
        /// El constructor vacío
        /// </summary>
        public Huellas()
        {
            this.fpLst = new List<Huella>();
        }

        public List<Huella> FpLst
        {
            get
            {
                return fpLst;
            }
        }

        /// <summary>
        /// Carga de huellas desde la base de datos
        /// </summary>
        public void loadFromDB()
        {
            string sqlStr = Properties.Settings.Default.sqlTemplates;
            string sqlWhere = Properties.Settings.Default.whereTemplates;

            sqlStr += " where ideTem>@ideTem";
            if (sqlWhere != "")
                sqlStr += " and " + sqlWhere;

            loadFromDB(sqlStr);
        }

        /// <summary>
        /// Carga de huellas desde la base de datos
        /// </summary>
        /// <param name="sqlStr">SQL a ejecutar</param>
        public void loadFromDB( string sqlStr )
        {
            MySqlDataReader rdr = null;
            MySqlConnection myConn = null;

            try
            {
                // Me conecto a la BD
                myConn = new MySqlConnection(connStr);
                myConn.Open();

                // Ejecuto el query
                Stopwatch st = new Stopwatch();
                st.Start();
                Console.WriteLine("Cargando las huellas a memoria ...");
                MySqlCommand cmd = new MySqlCommand(sqlStr, myConn);
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@ideTem", maxIdeTem);
                rdr = cmd.ExecuteReader();
                int cursorPos = 0;
                while (rdr.Read())
                {
                    string usuTem = rdr.GetString("usuTem");
                    int ideTem = rdr.GetInt32("ideTem");
                    Huella h = new Huella(
                    ideTem,
                    rdr.GetInt32("ideUsu"),
                    rdr.GetInt32("usuPer"),
                    rdr.GetInt32("ideDed"),
                    usuTem);
                    fpLst.Add(h);
                    ++cursorPos;
                    FingerprintTemplate tp = h.fpTemplate;
                    if (maxIdeTem <= ideTem)
                        maxIdeTem = ideTem;
                }
                st.Stop();
                Console.WriteLine("{0} Huellas cargadas en {1} ms", cursorPos, st.ElapsedMilliseconds);
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());
            }
            finally
            {
                timeLoaded = DateTime.Now;
                try
                {
                    if (rdr != null)
                        rdr.Close();
                    myConn.Close();
                }
                catch (Exception)
                {
                }
            }
        }

        /// <summary>
        /// Chequea si es necesario recargar la base de huellas
        /// En principio, chequea por tiempo, pero es necesario agregar
        /// algo mas sutil, como por ejemplo la cantidad de huellas.
        /// </summary>
        /// <returns>True si hay que recargar, False si no</returns>
        public bool loadNeeded()
        {
            int deltaT = Properties.Settings.Default.vigenciaHuellas;

            return timeLoaded.AddSeconds(deltaT) < DateTime.Now;
        }

        /// <summary>
        /// Comparacion multiple
        /// </summary>
        /// <param name="target">La huella leida del lector (o archivo si es simulacion)</param>
        /// <returns>La huella identificada o null</returns>
        public Huella compareTo(FingerprintTemplate target)
        {
            Huella ret = null;
            int maxScore = -1;
            int ctx = -1;
            try
            {
                FPCore.getInstance().CreateContext(out ctx);
                FPCore.getInstance().IdentifyPrepare(target, ctx);
                foreach (Huella h in fpLst)
                {
                    int hr = h.CompareTo(target, ctx);
                    Console.WriteLine("hr -> {0}", hr);
                    if (ret == null)
                        ret = h;
                    else
                    {
                        if (maxScore <= hr)
                        {
                            maxScore = hr;
                            ret = h;
                        }
                    }
                }
                ret.Score = maxScore;
            } catch(Exception e)
            {
                ret = null;
            }
            finally
            {
                if (ctx != -1)
                    FPCore.getInstance().DestroyContext(ctx);
            }

            return ret;

        }

        /// <summary>
        /// Particiona la lista de huellas para poder
        /// hacer una búsqueda en paralelo
        /// </summary>
        /// <param name="nPartes">La cantidad de particiones iguales</param>
        /// <returns>Un array de listas de huellas</returns>
        public Huellas[] getParts(int nPartes)
        {
            Huellas[] retVal = new Huellas[nPartes];
            int n = FpLst.Count / nPartes;
            for ( int i=0 ,  baseI =0; i< nPartes; i++ , baseI+=n )
            {
                retVal[i] = new Huellas();
                if (i == nPartes - 1)
                    if (baseI + n < FpLst.Count)
                        n = FpLst.Count - baseI;
                retVal[i].fpLst = FpLst.GetRange(baseI, n);
            }

            return retVal;
        }

        public static int GetIdeOpe(string SerialSensor)
        {
            String[] itemsSensores = Properties.Settings.Default.Sensores.Split('|');
            int ideope = 0;
            foreach (String item in itemsSensores)
            {
                String[] splitItem = item.Split(':');
                if (splitItem[0] == SerialSensor)
                {
                    ideope = Convert.ToInt32(splitItem[1]);
                }
            }

            return ideope;

        }


        public static void registrarAcceso(String SerialSensor, String ID, String perfil)
        {
            int IdeOpe = 0;
            String sDate = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day;
            String sTime = DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second;
            IdeOpe = GetIdeOpe(SerialSensor);
            if (IdeOpe != 0)
            {
                String sql = "INSERT INTO logs (IDELOG, IDEUSU, USUPER, IDEOPE, CODLOG, DATE, TIME, IDETER, IDEPERSEG, OBS) values (null," + ID + "," + perfil + "," + IdeOpe + ",1,'" + sDate + "','" + sTime + "'," + Properties.Settings.Default.Terminal + ",3,'')";
                executeQueryUpdate(sql);

                if (IdeOpe == 1 || IdeOpe == 2 || IdeOpe == 3 || IdeOpe == 4)
                {
                    String sql2 = "select max(IDELOG) as MAXIDELOG from logs";
                    DataTable dt = executeQuerySelectToDatatable(sql2);
                    int IdeLog = Convert.ToInt32(dt.Rows[0]["MAXIDELOG"]);
                    for (int i = 1; i < 5; i++)
                    {
                        String sql3 = "insert into checkviewer_msg (Id, IdLog, estado, serial, IdMonitor) values (null," + IdeLog + ",0,'" + SerialSensor + "'," + i + ");";
                        executeQueryUpdate(sql3);
                    }
                    // Abro el molinete
                    Reles.Open(IdeOpe);
                }
            }
        }

        internal static void executeQueryUpdate(String sql)
        {
            string cs = connStr;
            MySqlConnection conn = null;
            try
            {
                conn = new MySqlConnection(cs);
                conn.Open();
                                
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                Console.WriteLine("exec : {0}", sql);
                
                cmd.ExecuteNonQuery();

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            finally
            {
                if (conn != null)
                {
                    conn.Close();
                }
            }
        }

        internal static DataTable executeQuerySelectToDatatable(String sql)
        {
            string cs = connStr;
            MySqlConnection conn = null;
            conn = new MySqlConnection(cs);
            conn.Open();
            DataTable dt = new DataTable();

            MySqlCommand cmd = new MySqlCommand(sql, conn);
            using (MySqlDataAdapter sda = new MySqlDataAdapter(cmd))
            {
                dt = new DataTable();
                sda.Fill(dt);

            }
            return dt;
        }

    }
}
