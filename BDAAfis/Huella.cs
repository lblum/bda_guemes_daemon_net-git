﻿using GriauleFingerprintLibrary.DataTypes;
using System;

namespace BDA
{
    class Huella
    {
        private long FideTem = -1;
        private long FideUsu = -1;
        private long FusuPer = -1;
        private long FideDed = -1;
        private byte[] FusuTem;
        private FingerprintTemplate FfpTemplate = null;
        private int Fscore;

        public Huella(long fideTem, long fideUsu, long fusuPer, long fideDed, string fusuTem)
        {
            FideTem = fideTem;
            FideUsu = fideUsu;
            FusuPer = fusuPer;
            FideDed = fideDed;
            FusuTem = GetBytes(fusuTem);


            FingerprintTemplate vsTemp = new FingerprintTemplate();
            vsTemp.Size = usuTem.Length;
            vsTemp.Buffer = usuTem;
            vsTemp.Quality = 2;
            FfpTemplate = DLLImport.convertTemplate(vsTemp);

            FusuTem = FfpTemplate.Buffer;
        }

        public long ideTem
        {
            get
            {
                return FideTem;
            }
        }

        public long ideUsu
        {
            get
            {
                return FideUsu;
            }
        }

        public long usuPer
        {
            get
            {
                return FusuPer;
            }

        }

        public long ideDed
        {
            get
            {
                return FideDed;
            }
        }

        public byte[] usuTem
        {
            get
            {
                return FusuTem;
            }
        }


        public FingerprintTemplate fpTemplate
        {
            get
            {
                return FfpTemplate;
            }
        }

        public int Score
        {
            get
            {
                return Fscore;
            }
            set
            {
                Fscore = value;
            }
        }

        public int CompareTo(FingerprintTemplate vs,int ctx)
        {
            int score;
            int lw, hw;
            lw = Properties.Settings.Default.umbralLW;
            hw = Properties.Settings.Default.umbralHW;
            FingerprintTemplate fpT = FfpTemplate;

            try
            {
                int result = FPCore.getInstance().Identify(fpT, out score, ctx);
                if (score > lw && score < hw)
                {
                    // Es el caso dudoso. Hay que rechequear
                    result = FPCore.getInstance().Verify(fpT, vs, out score, ctx);
                }
            }
            catch (System.Exception e)
            {
                // Algún error, probablemente calidad
                score = -1;
            }

            return score;
        }

        // funciones auxiliares
        public static byte[] GetBytes(string str)
        {
            char[] ca = str.ToCharArray();

            int pos = str.Length - 1;

            int n = ca[3];

            //pos = n * 6 + 4;
            byte[] ret = new byte[pos + 1];

            for (int i = 0; i <= pos; i++)
            {
                ret[i] = (byte)ca[i];
            }
            return ret;
        }

        public static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        public override string ToString()
        {
            return string.Format("ideUsu = {0},usuPer = {1},ideDed = {2},ideTem = {3}", ideUsu, usuPer, ideDed, ideTem);
        }
}

}
