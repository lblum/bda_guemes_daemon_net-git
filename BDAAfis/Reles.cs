﻿using System;
using System.IO.Ports;
using System.Threading;

namespace BDA
{
    /// <summary>
    /// Singleton de acceso a la plaqueta de reles
    /// Hoy en día es un Arduino 1
    /// </summary>
    public class Reles
    {
        /// <summary>
        /// La instancia privada de la clase
        /// </summary>
        private static volatile Reles board = null;
        private static object syncRoot = new Object();

        /// <summary>
        /// El puerto serie autodetectado
        /// </summary>
        private readonly SerialPort sp = null;


        /// <summary>
        /// Constructor privado
        /// Autodetecta el puerto serie donde están los relés
        /// </summary>
        private Reles()
        {
            for (int i = 1; i < 10; i++)
            {
                Console.WriteLine("Autodetect intento " + i.ToString());
                if ((sp = Autodetect()) != null)
                    break;
                Thread.Sleep(500);
            }
        }

        /// <summary>
        /// Autodetección del puerto serie
        /// </summary>
        /// <returns></returns>
        private SerialPort Autodetect()
        {
            SerialPort retVal = null;
            foreach (var port in SerialPort.GetPortNames())
            {
                Console.WriteLine("Intentando el puerto {0}", port);
                try
                {
                    retVal = new SerialPort(port,9600);
                    retVal.ReadTimeout = 1000;
                    retVal.Open();
                    Thread.Sleep(100);
                    retVal.Write("?");
                    Thread.Sleep(500);
                    string resp = retVal.ReadLine();
                    try
                    {
                        Console.WriteLine("Respuesta -> " +resp);
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                    if (resp == "RELE")
                    {
                        Console.WriteLine("Placa de reles encontrada en el puerto {0}", port);
                        return retVal;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Excepcion -> "  + ex.Message);
                }
                finally
                {
                    try
                    {
                        retVal.Close();
                        retVal = null;
                    }
                    catch (Exception)
                    {
                    }
                }
            }

            // Si llegó acá es porque no hay placa disponible
            // TODO: ver si hay que abortar la aplicación o no
            Console.WriteLine("No se encontro placa de reles");
            return null;
        }


        private static Reles getInstance()
        {
            if (board == null)
            {
                lock (syncRoot)
                {
                    if (board == null)
                    {

                        board = new Reles();

                    }
                }
            }
            return board;
        }

        private static void _Cmd(string dir)
        {
            try
            {
                var sp = getInstance().sp;
                if (!sp.IsOpen)
                    sp.Open();

                sp.Write(dir);
                sp.Close();
            }
            catch (Exception exc)
            {
                Console.WriteLine("Error al acceder a la placa de reles: {0}", exc.Message);
            }

        }

        /// <summary>
        /// Apertura dependiendo del código de la base de datos
        /// TODO: Revisar esto (hay código 1 y 2)
        /// 3 -> Entrada
        /// 4 -> Salida
        /// </summary>
        /// <param name="dir"></param>
        public static void Open(int dir)
        {
            _Cmd(dir == 3 ? "1" : "2");
            _Cmd("4");
        }

        /// <summary>
        /// Lámpara de error
        /// TODO: Revisar esto (hay código 1 y 2)
        /// 3 -> Entrada
        /// 4 -> Salida
        /// </summary>
        /// <param name="dir"></param>
        public static void Error(int dir= 0)
        {
            _Cmd("4");
        }

    }
}
