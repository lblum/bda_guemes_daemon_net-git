const int pinMolineteIn = 2;
const int pinOkIn = 3;
const int pinErrIn = 4;

const int pinMolineteOut = 5;
const int pinOkOut = 6;
const int pinErrOut = 7;

// Relés
// Relé 1 -> DIO7
// Relé 2 -> DIO6
// Relé 3 -> DIO5
// Relé 4 -> DIO4

const int reles[]= { 7, 6 , 5 , 4 };
const int delayMolinete = 500;
const int delayLamparaOk = 3000;
const int delayLamparaError = 2000;

void setup() {
  Serial.begin(9600);
  resetAll();
}

void resetAll() {
  for (int i = 0; i < 5; i++) {
    pinMode(reles[i], INPUT);
    pinMode(reles[i], OUTPUT);
    digitalWrite(reles[i], LOW);
  }
}


void loop() {

    int option = Serial.read();
    switch(option) {
      case '1':
      case '2':
        digitalWrite(reles[option - '1'], HIGH);
        delay(delayMolinete);
        digitalWrite(reles[option - '1'], LOW);
        break;
      case '3':
      case '4':
        digitalWrite(reles[option - '1'], HIGH);
        delay(option=='4'? delayLamparaOk : delayLamparaError);
        digitalWrite(reles[option - '1'], LOW);
        break;
      case 'V':
      case 'v':
        Serial.write("v1.0\n");
        break;
      case '?':
        Serial.write("RELE\n");
        break;
      default:
        break;
    }
}
 